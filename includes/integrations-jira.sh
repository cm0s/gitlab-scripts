#!/bin/bash

integration_jira(){

  echo "Using following vars:"
  echo " - JIRA_URL: $JIRA_URL"
  echo " - JIRA_TOKEN: $JIRA_TOKEN"
  echo " - JIRA_USER: $JIRA_USER"
  echo " - JIRA_TID: $JIRA_TID"

  for REPO_ID in ${PROJECTS} ;do

    REPO_PATH="$(curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" "$URL/projects/$REPO_ID" | jq -r '.path_with_namespace')"
    if [ "x${DRY_RUN}" == "xtrue" ]; then


      echo "  curl -L --silent --header \"PRIVATE-TOKEN: $TOKEN\" \
        -X PUT \"$JIRA_URL/api/v4/projects/$REPO_ID/services/jira?jira_issue_transition_id=${JIRA_TID}&url=${JIRA_URL}&username=${JIRA_USER}&password=${JIRA_TOKEN}\""
    else
      echo -ne "\nid: ${REPO_ID}, ${REPO_PATH} "
      curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" \
        -X PUT "$JIRA_URL/api/v4/projects/$REPO_ID/services/jira?jira_issue_transition_id=${JIRA_TID}&url=${JIRA_URL}&username=${JIRA_USER}&password=${JIRA_TOKEN}"
      echo -ne " DONE \n"
    fi
  done
}
