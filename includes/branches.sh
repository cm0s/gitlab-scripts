#!/bin/bash

repo_branches_clean_purged(){

  for REPO_ID in ${PROJECTS} ;do

    REPO_PATH="$(curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" "$URL/projects/$REPO_ID" | jq -r '.path_with_namespace')"
    if [ "x${DRY_RUN}" == "xtrue" ]; then
      REPO_BRANCH="$(curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" "$URL/projects/$REPO_ID/repository/branches" | jq -r '.[].name')"

      echo -ne "\nid: ${REPO_ID}, ${REPO_PATH}\n"
      echo "  - branches"
      for branch in ${REPO_BRANCH}; do
        echo "    - $branch"
      done

    else
      echo -ne "id: ${REPO_ID}, cleaning merged branches: "
      curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" -X DELETE "$URL/projects/$REPO_ID/repository/merged_branches" |jq -r '.message'
    fi
  done
}
