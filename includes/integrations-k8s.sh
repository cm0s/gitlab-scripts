#!/bin/bash

integration_k8s(){

  echo "Using following vars:"
  echo " - K8S_URL: $GITLAB_URL"
  echo " - K8S_TOKEN: $K8S_TOKEN"
  echo " - K8S_USER: $K8S_USER"
  echo " - K8S_TID: $K8S_TID"

  for REPO_ID in ${PROJECTS} ;do

    REPO_PATH="$(curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" "$URL/projects/$REPO_ID" | jq -r '.path_with_namespace')"
    if [ "x${DRY_RUN}" == "xtrue" ]; then


      echo "  curl -L --silent --header \"PRIVATE-TOKEN: $TOKEN\" \
        -X DELETE \"$GITLAB_URL/api/v4/projects/$REPO_ID/services/kubernetes\""
    else
      echo -ne "\nid: ${REPO_ID}, ${REPO_PATH} "
      curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" \
        -X DELETE "$GITLAB_URL/api/v4/projects/$REPO_ID/services/kubernetes"
      echo -ne " DONE \n"
    fi
  done
}
