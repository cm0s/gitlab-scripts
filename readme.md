# Gitlab-Tools

* Contact: Roman v. Gemmeren <strowi@hasnoname.de>
* Url: <https://gitlab.com/strowi/gitlab-scripts>

A collections of useful gitlab-scripts:

* delete registry-tags without matching branches
* clone gitlab group/instance recursively
* delete already merged branches

## Usage

```bash
~> ./gl_cleanup.sh -u=https://$gitlabdomain/api/v4 -t=$TOKEN [-d] [-p=$project-id] [-r|-b]

  parameter:
    Limits:
      -p=$project-id        (default: all projects)
      -g=$group             limit operations to (sub-)group e.g. 'sys%2fmirror'

    Operations:
      -b|--branches         delete already merged branches
      -r|--registry         cleanup registry
      -l|--registry-list    list registry-images
    
    Others:
      -t|--token            user-token for Gitlab-api
      -u|--url              Gitlab-URL (e.g.: https://$gitlabdomain/api/v4)
      -d|--delete           really delete container-tags (DEFAULT: dry-run)
      -h|--help             this ;P

  requirements:
    - jq
    - curl
```

## Example

### Registry

This will remove container-tags without corresponding branch-names

```bash
~> ./gl.sh -t=$TOKEN -g=sys%2fmonitoring -r -u=https://git.chefkoch.net/api/v4
Limiting to: groups/sys%2fmonitoring
Subgroups:
DRY_RUN: true
NOT cloning repos
NOT cleaning merged branches

Working on registry-images without branches

id: 236, a/b/node-exporter, registry-id: true
  - branches:
    - master

  - imagetags:
    - 196/hcl-discovery 
       D| curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" -X DELETE https://git.chefkoch.net/api/v4/projects/236/registry/repositories/196/tags/hcl-discovery
    - 196/latest 
    - 196/master 
```

Which would delete the hcl-discovery tag from the registry-repo `196`.

### Merged Branches

This will remove already merged branches
```bash
~> ./gl_cleanup.sh --token=$token -u=https://git.chefkoch.net/api/v4 -b
Cleaning merged branches
id: 115, cleaning merged branches: 202 Accepted
id: 116, cleaning merged branches: 202 Accepted
id: 122, cleaning merged branches: 202 Accepted
id: 123, cleaning merged branches: 202 Accepted

```

### Clone Gitlab-Instance

This script can also clone a gitlab group recursively, mirroring the hierachy.

```bash
./gl.sh -t=$TOKEN -g=sys%2fmonitoring -c -u=https://git.chefkoch.net/api/v4
Limiting to: groups/sys%2fmonitoring
Subgroups:
DRY_RUN: true

Cloning Repos
= cloning into a/b/alertmanager ("ssh://$XYZ:2222/a/b/alertmanager.git")
= cloning into a/b/node-exporter ("ssh://$XYZ:2222/a/b/node-exporter.git")
= cloning into a/b/bind-exporter ("ssh://$XYZ:2222/a/b/bind-exporter.git")
= cloning into a/b/blackbox-exporter ("ssh://$XYZ:2222/a/b/blackbox-exporter.git")

```

